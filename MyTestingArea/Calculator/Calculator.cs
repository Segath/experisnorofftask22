﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class CalculatorClass
    {
        public static double Add(double x, double y)
        {
            return x + y;
        }

        public static double Subtract(double x, double y)
        {
            return x - y;
        }

        public static double Multiply(double x, double y)
        {
            double value = x * y;
            if(value >= double.MaxValue)
            {
                throw new OverflowException();
            }
            return value;
        }

        public static double Divide(double x, double y)
        {
            if(y == 0)
            {
                throw new DivideByZeroException();
            }
            return x / y;
        }

    }
}
