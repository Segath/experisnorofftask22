﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using System.Threading.Tasks;
using Calculator;

namespace myTests
{
    public class CalculatorTests
    {
        [Fact]
        public void Add_ShouldFindSumOfNumbers()
        {
            // Arrange
            double expected = 20;
            
            // Act
            double actual = CalculatorClass.Add(10, 10);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Divide_DivideByZeroException()
        {
            var ex = Assert.Throws<DivideByZeroException>(() => CalculatorClass.Divide(0, 0));
            Assert.IsType<DivideByZeroException>(ex);
        }

        [Fact]
        public void Multiply_OverflowException()
        {
            var ex = Assert.Throws<OverflowException>(() => CalculatorClass.Multiply(double.MaxValue, 2));
            Assert.IsType<OverflowException>(ex);
        }

        [Fact]
        public void Subtract_ShouldFindSumOfNumbers()
        {
            // Arrange
            double expected = 2;
            
            // Act
            double actual = CalculatorClass.Subtract(10, 8);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Multiply_ShouldFindSumOfNumbers()
        {
            // Arrange
            double expected = 100;

            // Act
            double actual = CalculatorClass.Multiply(10, 10);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Divide_ShouldFindSumOfNumbers()
        {
            // Arrange
            double expected = 10;

            // Act
            double actual = CalculatorClass.Divide(100, 10);

            // Assert
            Assert.Equal(expected, actual);
        }
    }


}
